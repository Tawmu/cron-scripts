<?php
require_once('config/config.php');
require_once('config/opnsense.php');

$OPNsense = new OpnSense($config['opnsense']);
$time = time();

try {
    $sqlConnection = mysqli_connect($config['db']['host'], $config['db']['username'], $config['db']['password'], $config['db']['database'], $config['db']['port']) or die("Database connection not established.");
    $insert = mysqli_query($sqlConnection, "INSERT INTO captiveportal_metrics (timestamp, type, data) VALUES (now(), 'sessioncount', {$OPNsense->captive_session_count()})");
    mysqli_close($sqlConnection);
} catch (mysqli_sql_exception $e) {
    echo "MySQLi Error Code: " . $e->getCode() . "<br />";
    echo "Exception Msg: " . $e->getMessage();
    exit; // exit and close connection.
}

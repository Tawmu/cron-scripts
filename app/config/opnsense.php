<?php

class OpnSense {
    /**
     * URL of the OPNsense instance
     * e.g. 'https://10.0.0.1'
     *
     * @var string
     */
    private $uri = "";

    /**
    * API key for connection to OPNsense
    *
    * @var string
    */
    private $apiKey = "";

    /**
    * API secret for connection to OPNsense
    *
    * @var string
    */
    private $apiSecret = "";

    /**
     * Allow insecure connections?
     *
     * @var bool
     */
    private $insecure = false;

    public function __construct($options = []) {
        $this->uri = $options['uri'];
        $this->apiKey = $options['apiKey'];
        $this->apiSecret = $options['apiSecret'];
        $this->insecure = (bool) $options['insecure'];
    }

    public function getInsecure() {
        return $this->insecure;
    }

    public function setInsecure($value) {
        $this->insecure = (bool) $value;
    }

    private function generate_auth_header() {
        $auth = base64_encode($this->apiKey.':'.$this->apiSecret);
        return "Authorization: Basic $auth";
    }

    private function build_uri($module, $controller, $command, $params = []) {
        return $this->uri.'/api/'.$module.'/'.$controller.'/'.$command.implode("/", $params);
    }

    private function opnsense_request($method, $module, $controller, $command, $params = [], $data = null) {
        $auth = $this->generate_auth_header();

        $opts = array(
            'http' => array(
                'method' => $method,
                'ignore_errors' => true,
                'header' => "${auth}\r\n",
            ),
        );

        if($method == 'POST') {
            $opts['http']['content'] = (empty($data))? null : json_encode($data);
            $opts['http']['header'] = "${auth}\r\nContent-Type: application/json\r\n";
        }

        if($this->insecure) {
            $opts['ssl']['verify_peer'] = false;
            $opts['ssl']['verify_peer_name'] = false;
            $opts['ssl']['allow_self_signed'] = true;
            $opts['ssl']['verify_host'] = false;
        }

        $context = stream_context_create($opts);
        $fp = fopen($this->build_uri($module, $controller, $command, $params = []), 'r', false, $context);
        $response  = stream_get_contents($fp);
        fclose($fp);

        $response = json_decode($response, true);
        return $response;
    }

    public function captive_add_session($user, $ip, $zone = 0) {
        return $this->opnsense_request('POST', 'captiveportal', 'session', 'connect', array($zone), array('user' => $user, 'ip' => $ip));
    }

    public function captive_delete_session($sessionId, $zone=0) {
        return $this->opnsense_request('POST', 'captiveportal', 'session', 'disconnect', array($zone), array('sessionId' => $sessionId));
    }

    public function captive_session_list($zone = 0) {
        return $this->opnsense_request('GET', 'captiveportal', 'session', 'list', array($zone));
    }

    public function captive_session_search($ip) {
        $sessionList = $this->captive_session_list();
        $user = '';

        foreach($sessionList as $session) {
            if(isset($session['ipAddress']) && $session['ipAddress'] == $ip) {
                $user = $ip;
            }
        }

        if($user) {
            return true;
        } else {
            return false;
        }
    }

    public function captive_session_count($zone = 0) {
        $sessions = $this->captive_session_list($zone);
        return count($sessions);
    }
}
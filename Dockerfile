FROM alpine:edge

ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.1.12/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=048b95b48b708983effb2e5c935a1ef8483d9e3e

RUN echo -n "https://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories && \
    apk update

RUN apk add --no-cache \
	curl \
    ca-certificates \
    supervisor \
    bash \
    tzdata \
    openssl \
    wget \
    curl \
    bash \
    fcgi \
    nano \
    tidyhtml \
    php81 \
    php81-pecl-apcu \
    php81-bz2 \
    php81-common \
    php81-curl \
    php81-dom \
    php81-exif \
    php81-fileinfo \
    php81-ftp \
    php81-gd \
    php81-iconv \
    php81-pecl-imagick \
    php81-mbstring \
    php81-mysqli \
    php81-openssl \
    php81-pdo \
    php81-posix \
    php81-soap \
    php81-session \
    php81-redis \
    php81-zip \
    php81-ldap \
    php81-bcmath \
    php81-calendar \
    php81-gettext \
    php81-pcntl \
    php81-phar \
    php81-simplexml \
    php81-shmop \
    php81-sysvmsg \
    php81-sysvsem \
    php81-sysvshm \
    php81-sockets \
    php81-tidy \
    php81-tokenizer \
    php81-xmlreader \
    php81-zip \
    php81-zlib \
    php81-xsl \
    php81-xml \
    php81-xmlwriter \
    php81-opcache \
    php81-ctype \
    php81-pdo_mysql \
    php81-pdo_sqlite \
    php81-sqlite3 \
    php81-intl \
    php81-fpm \
    php81-mysqli \
    php81-sodium && \

	ln -snf /usr/bin/php81 /usr/bin/php && \
	ln -snf /usr/sbin/php-fpm81 /usr/sbin/php-fpm && \

	# Symlink current version
	mkdir /etc/php && \
    sed -i -e 's|variables_order = "GPCS"|variables_order = "EGPCS"|g' /etc/php81/php.ini && \
	ln -snf /etc/php81 /etc/php/current;

# supercronic
RUN curl -fsSLO "$SUPERCRONIC_URL" \
 && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
 && chmod +x "$SUPERCRONIC" \
 && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
 && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic

# Composer
RUN EXPECTED_SIGNATURE="$(wget -q -O - https://composer.github.io/installer.sig)" && \
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    ACTUAL_SIGNATURE="$(php -r "echo hash_file('sha384', 'composer-setup.php');")" && \
    if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]; \
    then \
        >&2 echo 'ERROR: Invalid installer signature' \
        rm composer-setup.php \
    exit 1; \
    fi && \
    php composer-setup.php --quiet --install-dir=/usr/local/bin --filename=composer && \
    RESULT=$? && \
    rm composer-setup.php && \
    exit $RESULT

ADD app /app
ADD crontab /crontab

# Clear out garbage
RUN rm -rf /run/php && rm -rf /run/php-fpm8.1

ENTRYPOINT ["supercronic"]
CMD ["-passthrough-logs", "/crontab"]

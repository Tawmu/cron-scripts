<?php

$config['db']['host'] = $_ENV['db_host'];
$config['db']['port'] = $_ENV['db_port'];
$config['db']['database'] = $_ENV['db_name'];
$config['db']['username'] = $_ENV['db_username'];
$config['db']['password'] = $_ENV['db_password'];

$config['opnsense'] = [
    'uri' => $_ENV['opnsense_uri'],
    'apiKey' => $_ENV['opnsense_apikey'],
    'apiSecret' => $_ENV['opnsense_apisecret'],
    'insecure' => (bool) TRUE
];

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);